# Langkah Awal
## 1. build.gradle
### tambahkan
````
 //Navigation
    def nav_version = "2.3.0"
    implementation "androidx.navigation:navigation-fragment-ktx:$nav_version"
    implementation "androidx.navigation:navigation-ui-ktx:$nav_version"

    // RecyclerView
    implementation 'androidx.recyclerview:recyclerview:1.1.0'

    // Library CardView
    implementation 'androidx.cardview:cardview:1.0.0'

    // Glide
    implementation 'com.github.bumptech.glide:glide:4.11.0'
````

## 2. Masuk ke folder res -> layout
### buat progress_loading.xml
````
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="50dp"
    android:orientation="vertical">

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:gravity="center_horizontal"
        android:orientation="horizontal">

        <ProgressBar
            android:id="@+id/progressbar"
            android:layout_width="24dp"
            android:layout_height="wrap_content"
            android:layout_gravity="center_horizontal" />
    </LinearLayout>

</LinearLayout>
````
### buat item_list.xml
(https://gitlab.com/rizkysubagja2020/requestrecyclerviewfragment/-/blob/master/app/src/main/res/layout/item_list.xml)

## 3. Masuk folder java, tambahkan 3 package adapter, view, util. Masuk ke package util
### buat object Constant.kt
````
object Constant {
    //
    const val VIEW_TYPE_ITEM = 0
    const val VIEW_TYPE_LOADING = 1
}
````
### buat interface onLoadMoreScroll.kt
````
// create an interface where we are calling to load more data into our RecyclerView
interface OnLoadMoreListener {
    fun onLoadMore()
}
````
### buat RecyclerViewLoadMoreScroll.kt
(https://gitlab.com/rizkysubagja2020/requestrecyclerviewfragment/-/blob/master/app/src/main/java/com/example/requestrecyclerviewfragment/utils/RecyclerViewLoadMoreScroll.kt)

### 4. Fragment dan adapter
## RequestRecyclerViewFragmnet.kt
(https://gitlab.com/rizkysubagja2020/requestrecyclerviewfragment/-/blob/master/app/src/main/java/com/example/requestrecyclerviewfragment/view/RequestRecyclerViewFragment.kt)
## RequestRecyclerViewFragmentAdapter.kt
(https://gitlab.com/rizkysubagja2020/requestrecyclerviewfragment/-/blob/master/app/src/main/java/com/example/requestrecyclerviewfragment/adapter/RequestRecyclerViewAdapter.kt)




