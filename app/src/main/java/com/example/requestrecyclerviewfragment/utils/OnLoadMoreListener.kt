package com.example.requestrecyclerviewfragment.utils

// create an interface where we are calling to load more data into our RecyclerView
interface OnLoadMoreListener {
    fun onLoadMore()
}