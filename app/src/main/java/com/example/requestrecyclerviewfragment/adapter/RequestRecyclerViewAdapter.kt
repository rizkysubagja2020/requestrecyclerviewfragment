package com.example.requestrecyclerviewfragment.adapter

import android.content.Context
import android.graphics.BlendMode
import android.graphics.BlendModeColorFilter
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Build
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.requestrecyclerviewfragment.R
import com.example.requestrecyclerviewfragment.utils.Constant
import kotlinx.android.synthetic.main.item_list.view.*
import kotlinx.android.synthetic.main.progress_loading.view.*

class RequestRecyclerViewAdapter(private val itemCells: ArrayList<String?>)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    lateinit var mContext: Context

    // create class
    class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    class LoadingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    fun addData(dataViews: ArrayList<String?>) {
        this.itemCells.addAll(dataViews)
        notifyDataSetChanged()
    }

    fun getItemAtPosition(position: Int): String? {
        return itemCells[position]
    }

    // Add loading view
    fun addLoadingView() {
        Handler().post {
            itemCells.add(null)
            notifyItemInserted(itemCells.size - 1)
        }
    }

    // Remove loading view
    fun removeLoadingView() {
        if (itemCells.size != 0) {
            itemCells.removeAt(itemCells.size - 1)
            notifyItemRemoved(itemCells.size)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        mContext = parent.context
        return if (viewType == Constant.VIEW_TYPE_ITEM) {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.item_list, parent, false)
            ItemViewHolder(view)
        } else {
            val view =
                LayoutInflater.from(mContext).inflate(R.layout.progress_loading, parent, false)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                view.progressbar.indeterminateDrawable.colorFilter =
                    BlendModeColorFilter(Color.WHITE, BlendMode.SRC_ATOP)
            } else {
                view.progressbar.indeterminateDrawable.setColorFilter(
                    Color.WHITE,
                    PorterDuff.Mode.MULTIPLY
                )
            }
            LoadingViewHolder(view)
        }
    }

    override fun getItemCount(): Int {
        return itemCells.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (itemCells[position] == null) {
            Constant.VIEW_TYPE_LOADING
        } else {
            Constant.VIEW_TYPE_ITEM
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder.itemViewType == Constant.VIEW_TYPE_ITEM) {
            holder.itemView.item_textView.text = itemCells[position]
        }
    }

}