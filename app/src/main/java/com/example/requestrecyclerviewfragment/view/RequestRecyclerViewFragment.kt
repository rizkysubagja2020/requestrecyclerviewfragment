package com.example.requestrecyclerviewfragment.view

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.requestrecyclerviewfragment.R
import com.example.requestrecyclerviewfragment.adapter.RequestRecyclerViewAdapter
import com.example.requestrecyclerviewfragment.utils.OnLoadMoreListener
import com.example.requestrecyclerviewfragment.utils.RecyclerViewLoadMoreScroll
import kotlinx.android.synthetic.main.fragment_request_recycler_view.*

class RequestRecyclerViewFragment : Fragment() {

    lateinit var itemCells: ArrayList<String?>
    lateinit var loadMoreItemCells: ArrayList<String?>
    lateinit var adapterLinear: RequestRecyclerViewAdapter
    lateinit var scrollListener: RecyclerViewLoadMoreScroll
    lateinit var mLayoutManager: RecyclerView.LayoutManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_request_recycler_view, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Setup UI
        setItemData()
        setAdapter()
        setRecyclerViewLayoutManager()
        setRecyclerViewScrollListener()
    }

    private fun setItemData() {
        itemCells = ArrayList()
        for (index in 0..20) {
            // add Data
            itemCells.add("Item View $index")
        }
    }

    private fun setAdapter() {
        adapterLinear = RequestRecyclerViewAdapter(itemCells)
        adapterLinear.notifyDataSetChanged()
        item_list_recyclerView.adapter = adapterLinear
    }

    private fun setRecyclerViewLayoutManager() {
        mLayoutManager = LinearLayoutManager(requireContext())
        item_list_recyclerView.layoutManager = mLayoutManager
        item_list_recyclerView.setHasFixedSize(true)
    }

    private fun setRecyclerViewScrollListener() {
        mLayoutManager = LinearLayoutManager(requireContext())
        // scroll listener (RecyclerViewLoadMoreScroll.kt)
        scrollListener = RecyclerViewLoadMoreScroll(mLayoutManager as LinearLayoutManager)
        //
        scrollListener.setOnLoadMoreListener(object : OnLoadMoreListener {
            override fun onLoadMore() {
                loadMoreData()
            }
        })
        //
        item_list_recyclerView.addOnScrollListener(scrollListener)
    }

    private fun loadMoreData() {
        //Add the Loading View
        adapterLinear.addLoadingView()
        //Create the loadMoreItemsCells ArrayList
        loadMoreItemCells = ArrayList()
        //Get the number of the current Items of the main ArrayList
        val start = adapterLinear.itemCount
        //Load 16 more items
        val end = start + 16
        //Use Handler if the items are loading too fast.
        //If you remove it, the data will load so fast that you can't even see the LoadingView
        Looper.myLooper()?.let {
            Handler(it).postDelayed({
                //Your Code
                for (index in start..end) {
                    //Get data and add them to loadMoreItemsCells ArrayList
                    loadMoreItemCells.add("Item View $index")
                }
                //Remove the Loading View
                adapterLinear.removeLoadingView()
                //We adding the data to our main ArrayList
                adapterLinear.addData(loadMoreItemCells)
                //Change the boolean isLoading to false
                scrollListener.setLoaded()
                //Update the recyclerView in the main thread
                item_list_recyclerView.post {
                    adapterLinear.notifyDataSetChanged()
                }
            },3000)
        }
    }
}